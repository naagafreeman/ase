=======
Gallery
=======

Dissociation of oxygen on Pt(100)
=================================

.. image:: o2pt100.png
   :width: 10cm

:mod:`ase.io`
:download:`o2pt100.py`


Phase diagrams
==============

.. |p2| image:: ../ase/phasediagram/ktao-2d.png
   :width: 5cm
.. |p3| image:: ../ase/phasediagram/ktao-3d.png
   :width: 5cm

|p2| |p3|

:mod:`ase.phasediagram`
:download:`ktao.py <../ase/phasediagram/ktao.py>`


Brillouin zones
===============

.. |bzcubic| image:: ../ase/dft/cubic.svg
   :width: 25%
.. |bzbcc| image:: ../ase/dft/bcc.svg
   :width: 25%

|bzcubic| |bzbcc|

:mod:`ase.dft`
:download:`bz.py <../ase/dft/bz.py>`


Nudged elastic band calculations
================================

.. image:: ../tutorials/neb/diffusion-barrier.png
   :width: 10cm

:mod:`ase.neb`
:download:`barrier.py <../tutorials/neb/diffusion5.py>`
